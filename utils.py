import numpy as np
import cv2


def morphology_threshold(img, dilate_ksize=9):
    """
    method assumes input data comes with simmilar surface as table (e.g. not reflective)
    processing robust to multiple instance of nails objects (distanced from each other, without overlays)
    returns binarized img
    """
    blurred = cv2.GaussianBlur(img, (9,9), 0)
    _, threshed = cv2.threshold(blurred, 128,255, cv2.THRESH_TOZERO)
    erosed = cv2.erode(threshed, np.ones((3,3), np.uint8), iterations=1)
    dilated = cv2.dilate(erosed, np.ones((dilate_ksize, dilate_ksize), np.uint8), iterations=3)
    return dilated

def draw_contour_params(img):
    """
    function takes img with shape (width, height, 3),  preprocess it to
    binarized and returns contours with radius of enclosing circles on input img.
    """
    img_to_draw = img.copy()
    preprocessed = morphology_threshold(img)
    contours, _ = cv2.findContours(preprocessed, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)

    for c in contours:
        x, y, w, h = cv2.boundingRect(c)
        cv2.rectangle(
            img=img_to_draw, pt1=(x, y), pt2=(x + w, y + h),
            color=(0, 255, 0), thickness=2
        )
        rect = cv2.minAreaRect(c)
        box = cv2.boxPoints(rect)
        box = np.int0(box)
        cv2.drawContours(
            image=img_to_draw, contours=[box],
            contourIdx=0, color=(0, 0, 255)
        )

        (x, y), radius = cv2.minEnclosingCircle(c)
        center = (int(x), int(y))
        cv2.circle(
            img=img_to_draw, center=center, radius=int(radius),
            color=(255, 0, 0), thickness=2
        )
        cv2.putText(
            img=img_to_draw, text=str(int(radius)), org=center,
            fontFace=3, fontScale=5, thickness=10,
            color=(255, 0, 255)
        )
        
    return img_to_draw

def extract_roi_of_nail(raw_img):
    """
    function extract_roi extracts regions of interest (regions with instance of nail) on input raw img 
    using contour enclosing circle radius data. 
    
    Robust to multiple instance of nail objects
    """
    
    imgs = []
    preprocessed = morphology_threshold(raw_img)
    contours, _ = cv2.findContours(preprocessed, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
    
    for c in contours:
        (x, y), radius = cv2.minEnclosingCircle(c)
        if radius > 85 and radius < 185:
            center = (int(x), int(y))
            crop = raw_img[center[1] - 150:center[1] + 150, center[0] - 150:center[0] + 150]
            imgs.append(crop)
            
    return imgs
